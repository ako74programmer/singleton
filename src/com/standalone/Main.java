package com.standalone;

public class Main {
    public static void main(String[] args) {
        BillPughSingleton billPughSingleton = BillPughSingleton.getInstance();
        EagerSingleton eagerSingleton = EagerSingleton.getInstance();
    }
}
